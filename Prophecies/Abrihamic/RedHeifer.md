# Red Heifer
There is a belief that the third temple of Israel cannot be built until a red heifer is found to sacrifice. This belief comes from Numbers 19, which says:

> "This is the statute of the law that the Lord has commanded: Tell the people of Israel to bring you a red heifer without defect, in which there is no blemish, and on which a yoke has never come."
Numbers 19:2 ESV

and

> "And a man who is clean shall gather up the ashes of the heifer and deposit them outside the camp in a clean place. And they shall be kept for the water for impurity for the congregation of the people of Israel; it is a sin offering."
Numbers 19:9 ESV

Because a red heifer is important to the temple, it is assumed modern Israel would need one if they want to bulid a third temple. Currently, the Temple Institute is trying to create a perfect Red Heifer by embryo implantation. (See [here](link).)

## Selected Reading
[Numbers 19](https://www.biblegateway.com/passage/?search=Numbers+19&version=ESV)
